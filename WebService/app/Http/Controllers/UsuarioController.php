<?php

namespace App\Http\Controllers;

use App\usuario;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $data = new \stdClass;
        $data -> data = usuario::all();
        $data -> errors = "";
        return json_encode($data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function store(Request $request)
    {
        $data = new \stdClass;
        $data -> data = usuario::create($request -> all());
        $data -> errors = "este usuario existe";
        return json_encode($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(usuario $usuario)
    {
        $data = new \stdClass;
        $data -> data = usuario ::find($usuario);
        $data -> errors="no encuentra usuario";
        return json_encode($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usuario $usuario)
    {
        $data = usuario::findOrFail($usuario->id);
        $data-> update($request->all());
        return response($data)
        ->header('Content-Type','application/json');
        //return json_encode($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(usuario $usuario)
    {
        $data = new \stdClass;
        $data -> data = usuario:: findOrFail($usuario->id);
        $data-> data -> delete();
        return 204;
    }
}
